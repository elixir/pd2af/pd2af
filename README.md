> This project is a Fork from https://gitlab.com/pd2af/pd2af

# PD2AF Converter

PD2AF is a tool intended for translation of SBGN maps from Process Description to Activity Flow language.

## Project Navigation

* [REPOSITORY](https://git-r3lab.uni.lu/elixir/pd2af/pd2af) with all the relevant project's data
* [WEB INTERFACE](https://pd2af.lcsb.uni.lu/translator.html) for converter
* [SBGN EXAMPLES](https://git-r3lab.uni.lu/elixir/pd2af/pd2af/tree/master/knowledge/sbgn_examples/metabolismregulation.org) from *Metabolism Regulation project*
* [ISSUES](https://git-r3lab.uni.lu/elixir/pd2af/pd2af/issues) with current bugs and planned features
* [CONVERTER RULES](https://git-r3lab.uni.lu/elixir/pd2af/pd2af/-/blob/master/server/libs/odysseus_modules/pd2af/pd2af.rkt) implemented in Racket language


